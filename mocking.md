# Mocking

## Mocking API Response

```
public static func getQuestionnaireRequest(
    tenantId: Int,
    request: GetQuestionnaireRequestParameters,
    completion: @escaping ((_: Error?) -> Void)) {
    
        Wire.sessionManager.request(GetQuestionnaireRequestRouter.getQuestionnaireRequest(
        tenantId: tenantId,
        payload: request))
            .log()
            .validate(statusCode: 200 ..< 300)
            .apiManagerResponse { response in
                switch response.httpStatusCode {
                case 200 ..< 300:
                    // mocking using json file
                    if let file = Bundle.main.url(forResource: "stqQuestionaireData", withExtension: "json") {
                        do {
                            let data = try Data(contentsOf: file)
                            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                            
                            guard let result = GetQuestionnaireRequestResponse(json: json!) else {
                                return completion(WireError.parsing)
                            }
                            Parser.ARSkillsQuestion.parseSkillsQuestion(response: result, completion: { (error) in
                                completion(error)
                            })
                        }
                        catch {
                            
                        }
                    }
                    else {
                        
                    }
                    
                    // - - - - -
                    
//                    guard let result = GetQuestionnaireRequestResponse(json: response.jsonDictionary) else {
//                        return completion(WireError.parsing)
//                    }
//                    Parser.ARSkillsQuestion.parseSkillsQuestion(response: result, completion: { (error) in
//                        completion(error)
//                    })
//                    Parser.ARGoalProgressAndDetails.parseGoalProgressAndDetails(response: result, completion: { error in
//                        completion(error, result)
//                    })

                case 400:
                    return completion(response.result.error ?? BackendError.other, nil)
                
                default:
                    return completion(response.result.error ?? BackendError.other)
            }
        }
    }
```
	
```
public static func getMockHomeScreen(
	tenantId: Int,
	partyId: Int,
	vitalityMembershipId: Int,
	completion: @escaping (_ error: Error?) -> Void) {
        if let path = Bundle.main.url(
        	forResource: "getHomeScreen_AWC",
        	withExtension: "json") {
            if let stubResponse = parseJsonFile(url: path){
                guard let result = GetHomeScreenResponse(json: stubResponse) else {
                    return completion(WireError.parsing)
                }
                Parser.HomeScreen.parseGetHomeScreen(response: result, completion: completion)
                return
            }
            else {
                print("No file!")
            }
        }
        return completion(nil)
    }
```

**IMPORTANT:**

* The mock response json **file directory** should be in the `VitalityKit / Routers / Stub`
* The json file above should be a **target membership** under `VitalityAdvance`