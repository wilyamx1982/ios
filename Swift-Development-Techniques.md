# iOS Swift Development Techniques

## Sorting

```
extension String {
	func toDouble() -> Double {
        if let value = Double(self) {
            return value
        }
        return 0
    }
}
	
// Default sorting
let listSorted = list.sorted(
    by: {
        $0.range?.components(separatedBy: String.space).first?.toDouble() ?? 0 <
            $1.range?.components(separatedBy: String.space).first?.toDouble() ?? 0
    }
)
```

```
func filterOrdersByDate(order:[PhleboTask2], dates: [Date]) -> [PhleboTask2] {
    let startDate = Date().toStartDate(from: String(describing: dates[0]))
    let endDate = Date().toEndDate(from: String(describing: dates[1]))
    
    let filterResultArray = order.filter {
        let orderdate = $0.orderDateTime?.components(separatedBy: "T").first ?? ""
        return orderdate.toDate() >= startDate && orderdate.toDate() < endDate
    }
    let appendTasks: [PhleboTask2] = filterResultArray
    return appendTasks
}
```

```
public func calculatedRanges(
    clinicCoordinate: CLLocationCoordinate2D,
    riderList: [riderPhleboDispatch]) -> [riderPhleboDispatch] {
        
    ihhLogger.info(message: "[Distance] Clinic Coordinate. Lat: \(clinicCoordinate.latitude), Long: \(clinicCoordinate.longitude)")
    
    var list = riderList
        
    for index in 0..<list.count {
        let rider = list[index]
        guard let onDuty = rider.onDuty, onDuty == 1 else {
            list[index].range = String.offline
            continue
        }
        guard let riderLocation = rider.location else {
            list[index].range = EMPTY_RANGE_DISPLAY
            continue
        }
        guard let latitude = riderLocation.latitude else {
            list[index].range = EMPTY_RANGE_DISPLAY
            continue
        }
        guard let longitude = riderLocation.longitude else {
            list[index].range = EMPTY_RANGE_DISPLAY
            continue
        }
        
        let clinicLocation = CLLocation(
            latitude: clinicCoordinate.latitude,
            longitude: clinicCoordinate.longitude
        )
        let riderLoctn = CLLocation(
            latitude: Double(latitude),
            longitude: Double(longitude)
        )
        
        let distanceInMeters = riderLoctn.distance(from: clinicLocation)
        let distanceInKilometers = Double(distanceInMeters / 1000.0)
        
        let distanceInKilometers2 = distanceInKilometers.rounded(toPlaces: 2)
        list[index].range = "\(distanceInKilometers2) KM"
        
        //ihhLogger.info(message: "[Distance] from Rider (\(rider.riderID ?? 0)), Range: \(distanceInKilometers2) KM!")
    }
    
    // Default sorting
    let listSorted = list.sorted(
        by: {
            $0.range?.components(separatedBy: String.space).first?.toDouble() ?? 0 <
                $1.range?.components(separatedBy: String.space).first?.toDouble() ?? 0
        }
    )
        
    let invalidRange = listSorted.filter({
        $0.range == EMPTY_RANGE_DISPLAY
    })
    let invalidRangeSorted = invalidRange.sorted(by: {
        ($0.fullName ?? String.dash).lowercased() < ($1.fullName ?? String.dash).lowercased()
    })
    
    let validRange = listSorted.filter({
        $0.range != EMPTY_RANGE_DISPLAY && $0.range != String.offline
    })
        
    let offline = listSorted.filter({ $0.range == String.offline })
    let offlineSorted = offline.sorted(by: {
        ($0.fullName ?? String.dash).lowercased() < ($1.fullName ?? String.dash).lowercased()
    })
        
    return validRange + invalidRangeSorted + offlineSorted
}
```