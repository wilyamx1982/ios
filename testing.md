# iOS Testing

## iOS Unit Testing

To test an app, one needs to write three different kinds of test code.

1. **Unit Test** — Test the **single function without any external dependency**.
	
	e.g. to test the given email is valid or not.

1. **Integration Test** — Test the **single function with dependency**.

	e.g., to test that the value from the server response is handled for nil case too.

1. **UI Test** — Automate the user behavior and interaction with the app.

	e.g., On behalf of the user, trigger the “Signup” button action to validate the fields.


### What can you test with Unit Testing?

* **Functions and methods:** Test individual functions and methods to ensure they return expected outputs for given inputs and handle edge cases appropriately.

* **Performance:** Test the performance of your code by measuring the time taken to execute specific tasks or functions. This helps you identify bottlenecks and optimize your code.

* **Asynchronous code:** Test asynchronous code, such as network requests and completion handlers, to ensure they complete successfully and return expected results.

* **Data models:** Test the behavior and consistency of your data models, including validation, serialization, and deserialization.

* **Business logic:** Test the core logic of your application to ensure it meets the required specifications and behaves as expected.

* **Code Coverage:** Measure and analyze the percentage of your Xcode code coverage covered by your unit tests to ensure a high level of test coverage.

* **Mocking and stubbing:** Use mock objects and stubs to isolate specific components during testing, which helps control dependencies and focus on the component under test.

* **UI components:** While unit tests primarily focus on non-UI code, you can also test some aspects of your UI components, such as their initial state and interaction with other objects.


## Health App

1. `VIAHealthKit / Classes / VITHealthKitHelper+HealthKitQueriesh.m`

		/**
		 Note: Use only this bundle identifier for Debug-Simulator Purposes
		 NSString * const kAppleHardwareDeviceBundleIdentifierPrefix = @"com.apple.Health";
		 
		 For actual devices use:
		 NSString * const kAppleHardwareDeviceBundleIdentifierPrefix = @"com.apple.health";
		 */
		
1. Add some **data** for `Steps` to Simulator's **Health app**