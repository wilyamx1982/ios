# Tools (edit)

1. iOS Development
	* XCode
	* SF Symbols
	* Transporter
	* MacSymbolicator
	* Realm Studio
	* Postman
	* [Homebrew](https://brew.sh/)
	* [Cocoapods](https://cocoapods.org/)
	
1. Source Version Control
	* Sourcetree
	
1. Web Browser
	* Google Chrome

1. Text Editor
	* [MacDown](https://macdown.uranusjr.com/)
	* Sublime Text
	
1. Team Collaboration
	* Teams

# [Homebrew](https://brew.sh/)
```
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
	
$ brew --version
Homebrew 4.1.21-40-g942e77d
Homebrew/homebrew-core (git revision ec4b1fa3ed; last commit 2021-02-20)
```
```
$ sudo chown -R administrator /usr/local/share/zsh /usr/local/share/zsh/site-functions
$ sudo chown -R administrator /usr/local/share/zsh /Library/Ruby/Gems/2.6.0
```

1. [Installation](https://apple.stackexchange.com/questions/419724/homebrew-installation-failed)

		# M1/M2 system
		sudo rm -rf /opt/homebrew
		
1. [Official site](https://brew.sh/)

		/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

1. Follow the **next steps** to execute commands in terminal

1. [Phrase](https://support.phrase.com/hc/en-us/articles/5808300599068-Using-the-CLI-Strings-) app installation

		brew install phrase
		
		$ which phrase
		/opt/homebrew/bin/phrase

		$phrase version
		2.6.5

1. **~/.bash_profile**

		export PHRASEAPP_TOKEN=
	
1. `source ~/.bash_profile`

1. `printenv PHRASEAPP_TOKEN`

1. Edit **fastlane / Fastfile**

	* Find: `--access-token`, Replace All: `--access_token`
	* Find: `phraseapp pull` Replace All: `phrase pull`
	* You may apply the **GIT Stash** `phrase-cli`
	
1. `fastlane`

	* `ios phraseapp_caa` - Downloads latest strings files from PhraseApp for **Central Advance**
	* `ios phraseapp_ca` - Downloads latest strings files from PhraseApp for **Central Asset**
	* `ios phraseapp_all` - Downloads all the latest strings files from PhraseApp for **All Targets** (optional)

1. XCode build

# Cocoapods for 3rd-Party Libraries

	# 1.8.4
	sudo gem install -n /usr/local/bin cocoapods -v 1.8.4
	
	Williams-MacBook-Pro:LIG wrena$ pod --version
	1.8.4
	
	Williams-MacBook-Pro:LIG wrena$ which pod
	/usr/local/bin/pod

	---
	
	ruby --version
	ruby 3.1.2p20 (2022-04-12 revision 4491bb740a) [x86_64-darwin21]
	
	---
	
	sudo gem uninstall cocoapods
	sudo gem uninstall cocoapods-core
	sudo gem uninstall cocoapods-downloader
	sudo rm -rf ~/.cocoapods
	
	---
	
	pod --version
	1.8.4
	
	sudo gem install -n /usr/local/bin cocoapods -v 1.11.2
	1.11.2

### Fastlane Setup

1. [Fastlane setup](https://docs.fastlane.tools/getting-started/ios/setup/)
	
		$ ruby --version
		ruby 2.6.10p210 (2022-04-12 revision 67958) [universal.arm64e-darwin22]
		
		$ sudo gem install fastlane

		$ which fastlane
		/usr/local/bin/fastlane

		$ fastlane --version
		fastlane 2.211.0
		
# SourceTree for Git Client

1. [Download](https://www.sourcetreeapp.com/) and install **SourceTree app**.

2. [Add accounts](https://bitbucket.org/wilyamx1982/git/src/master/) for [GitHub](https://github.com/) or [Bitbucket](https://bitbucket.org/) repositories. This will create **Remote repository link for cloning**.

3. Setup an **SSH key**. [Guide here](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/)
	
		$ ssh-keygen
		$ cd ~/.ssh
		...
		$ ls
		config
		id_rsa
		id_rsa.pub
		
4. **Add public key** to your repository account settings. **Avoid setting password**.

		$ pbcopy < ~/.ssh/id_rsa.pub 
	
	Github and Bitbucket will have the **same SSH key to use**.
	
# Installation

	sudo spctl --master-disable
	sudo spctl --master-enable

# SwiftyBeaver for Debug Log

1. [Basic setup](https://docs.swiftybeaver.com/article/6-basic-setup)
2. [CocoaPods](https://cocoapods.org/pods/SwiftyBeaver)

## XCode

1. Simulator

		OS_ACTIVITY_MODE = disable
	
2. Image Export

	* Export: SVG
	* Image Set
		* Resizing: Preserve Vector Data
		* Scales: Single Scale
		* Universal
		 	
# Useful Links

* [Apple Developer Account](https://developer.apple.com/)
* [Apple ID](https://appleid.apple.com/account#!&page=create)
* [AppStore Connect](https://appstoreconnect.apple.com/)
* [Cocoa Pods](https://guides.cocoapods.org/)
* [Firebase Crashlytics](https://firebase.google.com/docs/crashlytics/customize-crash-reports)
* [Human Interface Guidelines](https://developer.apple.com/design/human-interface-guidelines/ios/overview/themes/)
* [Mixpanel APNS Certificate](Mixpanel-Update-APNS-Certificate.md)
* [More Downloads for Apple Developers](https://developer.apple.com/download/more/)
* [SendBird Error Codes](https://sendbird.com/docs/chat/v3/ios/guides/error-codes)
* [SendBird Framework Reference](https://docs.sendbird.com/ref/ios/index.html)
* [SendBird SDK (iOS)](https://docs.sendbird.com/ios)
* [Swift Language Guide](https://docs.swift.org/swift-book/LanguageGuide/TheBasics.html)
* [Webservices](iOS-Webservices.md)