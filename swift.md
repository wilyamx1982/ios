# Swift Best Practices

## Object Oriented Programming

* A computer programming model that organizes software design around data, or objects, rather than functions and logic. An object can be defined as a data field that has unique attributes and behavior.
* Contain data and code: data in the form of fields (often known as attributes or properties), and code in the form of procedures (often known as methods).

### Principles of OOP

* **Encapsulation** - states that all important information is contained inside an object and only select information is exposed. 
* **Abstraction** - objects only reveal internal mechanisms that are relevant for the use of other objects, hiding any unnecessary implementation code. 
* **Inheritance** - classes can reuse code from other classes. 
* **Polymorphism** - objects are designed to share behaviors and they can take on more than one form. Polymorphism allows different types of objects to pass through the same interface.

## Swift Language

### Classes

* Classes are **reference type**. When we initialize an object, RAM allocates memory space and address to it. Then, **assigns its memory address to the object** we created.

### Protocols

Define requirements that conforming types must implement.

A protocol defines a **blueprint of methods, properties, and other requirements that suit a particular task or piece of functionality.** The protocol can then be adopted by a **class**, **structure**, or **enumeration** to provide an actual implementation of those requirements. Any type that satisfies the requirements of a protocol is said to **conform** to that protocol.

### Optional Chaining

Access members of an optional value without unwrapping.

### Closures

Group code that executes together, without creating a named function.

### Enumerations

Model custom types that define a list of possible values.

### Extensions

Add functionality to an existing type.

### Delegate

Any object that should be notified when something interesting has happened. 

### Inheritance

Subclass to add or override functionality.

### Structures

* Structs are **value type**. A value type is a type whose **value is copied when it's assigned to a variable or constant, or when it's passed to a function**.
* Use structures when you're modeling data that contains information about an entity with an identity that you don't control.
* Use structures and protocols to **model inheritance** and **share behaviour**.

### Codable

* is used to **encode and decode custom data formats**, such as JSON, to native Swift objects and vice versa
* is actually a type alias that combines two protocols - **Encodable** (swift data type to JSON format) and **Decodable** (JSON format to Swift datatype)

### Strong vs Weak vs Unowned

* Strong references in Swift **increment the reference count by 1**. 
* Weak references are one solution to r**etain cycles in Swift**. A weak reference **does not increment or decrement the reference count of an object**. A **weak reference can be nil.**
* Life a weak reference, an unowned reference does not increment or decrement the reference count of an object. However, unlike a weak reference, the program guarantees to the Swift compiler that an **unowned reference will not be nil when it is accessed.**

### Properties

**Stored properties** store constant and variable values as part of an instance, whereas **computed properties** calculate (rather than store) a value.

### Generics

Write code that **works for multiple types** and specify requirements for those types.

## Networking with URLSession + Completion Handlers

The **URLSession class** and related classes provide an API for downloading data from and uploading data to endpoints indicated by URLs. Your app can also use this API to perform background downloads when your app isn’t running or, in iOS, while your app is suspended. 

**Making an API call** using URLSession. Implement **completion handlers** using escaping closures.

## Concurrency

* Perform asynchronous operations.
* In computer programming, **asynchronous operation** means that a process operates independently of other processes, whereas **synchronous operation** means that the process runs only as a result of some other process being completed or handed off.

## MVVM Architecture Design Pattern

Is a **structural design pattern**. MVVM allows you to use data from a single model class and represent it in different ways to populate a view. [Reference](https://www.freecodecamp.org/news/an-overview-of-the-mvvm-design-pattern-in-swift-fb815ea5da40/)

**Models** - These **hold the app data**. These are the **structs** and **classes** that you have created to hold the data you receive from a REST API or from some other data source.

**Views** - These display **UI elements on the screen**. These are usually classes that subclass UIView and use UIKit.

**View Models** - **It is responsible for all business logic.** These classes are where you take the **information from the model classes and transform them into values that can be displayed in a particular view**. It receives information from the ViewController, processes all this information & sends the results back to ViewController.

## Data Binding in MVVM on iOS

**Data Binding** is simply the process that establishes a **connection between the app UI (View Controller)** and **the data (Not Model, But View Model) it displays**. There are different ways of data binding so we’ll look at a couple. Please note, that Data Binding does not apply only to MVVM but to other patterns too. The ultimate goal of MVVM is to isolate the UI logic responsibility of ViewController to ViewModel. In addition, ViewModel hides data preparation code for visual presentation, asynchronous networking code as well as code listening for model updates. Further, when the ViewModel needs to communicate anything to the View, it will accomplish this with data bindings.

1. **Observables**
1. **Event Bus / Notification Center**
1. **FRP** (Functional Reactive Programming) **Technique** (Reactive Cocoa / RxSwift)
1. **Combine Framework** (unified publish-and-subscribe API for channeling and processing asynchronous signals)
1. **Escaping Closures**

## Unit Testing

Test the single function without any external dependency. e.g. to test the given email is valid or not

### What is Test Driven Development (TDD)?

* Test Driven Development is software development approach in which **test cases are developed to specify and validate what the code will do.**
* In simple terms, **test cases for each functionality are created and tested first and if the test fails then the new code is written in order to pass the test and making code simple and bug-free.**
* Test-Driven Development starts with designing and developing tests for every small functionality of an application.
* TDD framework instructs developers to write new code only if an automated test has failed.

A **stub** just **returns fake data for our tests**. That’s all.

A **mock** is slightly more complex than a stub. It **returns some fake data and can also verify whether a particular method was called.**

**When mocking an object**, we are essentially creating a "fake" version of it - with the same API as the real one - in order to more easily be able to assert and verify outcomes in our test cases.

## Benefits of Modularization or Componentization

**Framework** - self-contained, reusable chunks of code and resources you can import into many apps.

**Swift Package Manager (SPM)** - is a tool for managing the distribution of Swift code

**CocoaPods** - is a dependency manager for Swift and Objective-C Cocoa projects

1. Code is easier to read
1. Reusability without bloat
	* portable to other projects
1. Easier to collaborate
1. Decreases build time

## Data Storage

1. **Keychain** - a secure alternative to saving sensitive data, such as user names and passwords
1. **User Defaults** - simple and effective for saving small, simple bits of data
1. **Core Data** - Persist or cache data on a single device, or sync data to multiple devices with CloudKit.
1. **Realm Database** - a fast, scalable alternative to SQLite with mobile to cloud data sync that makes building real-time, reactive mobile apps easy.
1. **Swift Data** - SwiftData makes it easy to persist data using declarative code. You can query and filter data using regular Swift code. And it’s designed to integrate seamlessly with SwiftUI.

## SOLID Design Principle

These principles establish practices that lend to developing software with considerations for maintaining and extending as the project grows. Adopting these practices can also contribute to avoiding **code smells**, **refactoring code**, and Agile or Adaptive software development.

1. **Single Responsibility Principle** (SRP)

	States that a class, module, or function should have only one reason to change, meaning it should do one thing.

1. **Open-Closed Principle** (OCP)

	States that classes, modules, and functions should be open for extension but closed for modification.

1. **Liskov Substitution Principle** (LSP)

	Child classes or subclasses must be substitutable for their parent classes or super classes. In other words, the child class must be able to replace the parent class. This has the advantage of letting you know what to expect from your code.

1. **Interface Segregation Principle** (ISP)

	States that clients should not be forced to implement interfaces or methods they do not use.

1. **Dependency Inversion Principle** (DIP)

	The principle states that high-level modules should not depend on low-level modules. Instead, they should both depend on abstractions. Additionally, abstractions should not depend on details, but details should depend on abstractions.