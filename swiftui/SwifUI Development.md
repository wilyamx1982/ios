# SwiftUI Development Notes

## NavigationLink

```
NavigationLink(destination: RiderActivityConfirmPickup(taskStore: taskStore),
               isActive: $taskStore.showConfirmPickup) {
    EmptyView()
}

NavigationLink(destination:
    RiderActivityClinicPickUpTaskDetailsView(
        taskStore: RiderActivityClinicPickUpTaskDetailsStore(
            details: store.taskClinicPickUpAssignDetails)
    ),
               isActive: $store.showPickUpDetails,
               label: {
    EmptyView()
})
```
```
ForEach(0..<$listStore.filteredOrders.count, id:\.self){ index in
	NavigationLink(destination: {
    	let selectedOrder = listStore.filteredOrders[index]
    	LabTestOrderJourneyClinicCollectionView()
	}) {
   		LabTestOrderOngoingRowView(labOrder: listStore.filteredOrders[index])
	}
}
```
```
ScrollView(.vertical) {
    ForEach(store.getClinicCompletedList(), id: \.pickUpID) { detail in
        NavigationLink(destination: {
            Text("Details")
        }) {
            RiderTaskClinicCompleteRowView(pickUpDetails: detail)
        }
    }
}
```
```
ScrollView(.vertical) {
    ForEach(store.activityHistoryPaginated, id: \.taskID) { task in
        Button {
            ihhLogger.info(message: "Get Details! \(task.taskID ?? 0)")
        }
        label: {
            NavigationLink(
                destination:
                    PSupervisorTaskTrackingDetailView(
                        store: TaskTrackingstore(taskId: task.taskID ?? 1),
                        statusType: $store.selectedTab
                    )
                ,
                isActive: $store.showDetails) {
                    PhleboSupervisorActivityHistoryRowView(
                        task: task,
                        taskTab: store.selectedTab
                    )
            }
        }
    }
}
```

## Button

```
Button {
	//
}
label: {
    Text("Start")
        .fontWeight(.bold)
        .frame(height: 30)
        .font(.caption)
        .foregroundColor(.white)
}
.frame(maxWidth: .infinity)
.background(Color.tuntasBlue)
.disabled(!allowStart())
.overlay(!allowStart() ? Color.tuntasLightGray.opacity(0.5) : Color.clear)
.cornerRadius(5)
```

## List

```
ScrollView(.vertical) {
    ForEach(0..<5, id: \.self) { index in
        RiderTaskClinicCompleteRowView()
    }
}
```

```
ScrollView(.vertical) {
    ForEach(store.activityList, id: \.orderID) { activity in
        let request = PhleboRequest()
        Button {
            ihhLogger.info(message: "Selected an order: \(request.orderID ?? 0)")
        }
        label: {
            ActivityHistoryPhlebotomistRowView(task: request)
        }
    }
    
    Spacer()
        .frame(minHeight: .frame(.px10))
}
```

## Notification Center

```
extension NSNotification {
	static let CloseClinicPickUpDetails = NSNotification.Name.init("CloseClinicPickUpDetails")
}
```

```
NotificationCenter.default.post(name: NSNotification.CloseClinicPickUpDetails,
                                object: nil)
```

```                                                   
var body: some View {
	...
}
.onReceive(NotificationCenter.default.publisher(
            for: NSNotification.CloseClinicPickUpDetails)) { obj in
    store.showPickUpDetails = false
}
```

## Change Struct values

```
func selectAllCheckboxTasks() {
    for index in 0..<checkboxTasks.count {
        checkboxTasks[index].state = true
    }
    //ihhLogger.info(message: "Checkbox Tasks: \(checkboxTasks)")
}
```

## View Methods

```
@ViewBuilder
private var riderIncidentView: some View {
     HStack(spacing: .padding(.px7)) {
     	...
     }
}
```
```
func RiderRemarksView() -> some View {
   	  VStack {
   	  	...
   	  }
}
```