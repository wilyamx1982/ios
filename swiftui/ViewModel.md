# IHH View Model Structure

## View Model

```
import Foundation
import Combine

final class LabTestOrderClinicCollectionStore: ObservableObject {
	private var labService: LabService
   	private var orderDetailsSubscriber : AnyCancellable?
   	
   	@Published var orderDetail: LabOrderDetail = LabOrderDetail(orderID : 0)
}

extension LabTestOrderClinicCollectionStore {
	func getOrderDetails(orderId: Int) {
        labService.getTestOrderDetails(orderID: orderId)
        
        orderDetailsSubscriber = labService.$orderDetailRequest
            .receive(on: DispatchQueue.main)
            .sink{ result in
                switch result {
                
                case .failure(let error):
                    ihhLogger.logForApiError(error: error)
                    break
                    
                case .success(let orderDetail):
                    self.orderDetail = orderDetail!
                    self.evaluateSpecimens()
                    break
                    
                case .none:
                    break
                }
            }
    }
 
}

```

## Networking

```
final class LabService {
    @Published var orderDetailRequest: Result<LabOrderDetail?, Error>?
}

extension LabService {
	func getTestOrderDetails(orderID: Int) {
        do {
            let apiRequest = ApiRequest(securityHeader: ApiService().getSecurityHeaders(), orderID: orderID)
            let request = try JSONEncoder().encode(apiRequest)
            
            ApiService().post(path: ApiEndPoints.labDashboardTestOrderDetails, bodyParam: request) { result in
                switch result {
                case .success(let data):
                    do{
                        let response = try JSONDecoder().decode(ApiResponse.self, from: data)
                        if(response.responseMessage == String.success){
                            self.orderDetailRequest =  .success(response.order)
                        } else {
                            self.orderDetailRequest = .failure(ApiError.dataNotFound)
                        }
                    } catch  {
                        self.orderDetailRequest = .failure(ApiError.parsingError)
                    }
                    
                case .failure(_):
                    self.orderDetailRequest = .failure(ApiError.badRequest)
                }
            }
        } catch {
            ihhLogger.error(message: "Encoding")
        }
    }
}
```