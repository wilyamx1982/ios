# Binding UI List

### ViewModel: RiderActivityConfirmStore
```
final class RiderActivityConfirmStore: ObservableObject, ViewModelMockupProtocol {
    @Published var specimensInventory: [SpecimenInventory] = []
}
```

### View: RiderActivitySampleCollectionChecklist

```
struct RiderActivitySampleCollectionChecklist: View {
    @Binding var specimens: [SpecimenInventory]
    
    var body: some View {
    	VStack(spacing: 0) {
    	
	    	ForEach($specimens, id: \.self.name) { $item in
		        RiderSpecimenInventoryRowView(specimen: item.name,
		                                      quantity: $item.quantity)
		    }
    	
    	}.background(Color.white)
            .padding(.horizontal, 20)
    }
    
```

### RowView: RiderSpecimenInventoryRowView

```
struct RiderSpecimenInventoryRowView: View {

    @Binding var quantity: String
    
    var body: some View {
        HStack {
            TextField("", text: $quantity)
                .font(.subheadline.bold())
                .frame(width: 100, height: 30)
                .background(Color.tuntasMidGray.opacity(0.2))
                .overlay(
                    RoundedRectangle(cornerRadius: 2)
                        .stroke(Color.tuntasMidGray, lineWidth: 1)
                )
                .multilineTextAlignment(.center)
                .padding(.trailing, 20)
                
        }.padding(10)
    }
    
}
```