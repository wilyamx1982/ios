# SwiftUI Data Flow

## Structs

* Character, String
* Integer, Float
* Double, Boolean
* Array, Dictionary
* Set, Tuple
* All have fixed values

## Property Wrappers

* Has a **projected value** that is binding and we access that value with the money sign
* There is a **binding** inside using **dollar sign**
* [How to create custom Property Wrappers in SwiftUI (PART 1/2) | Advanced Learning #30](https://www.youtube.com/watch?v=2wzq6SQkSJE&list=PLwvDm4Vfkdphc1LLLjCaEd87BEg07M97y&index=31)

## @State Properties

* View code reads state property
* Store values that the view depends on
* Represent values that can change and when it does, the view recomputes itself using the new value
* Typically declared in the view that needs them
* One way (read) relationship

## @Binding

* Put a $ in front of a state property reference in your view code (not in the declaration) to turn it into a two way, (read/write) relationship
* Property wrapper that it expects a binding to be passed to the view when an instance of the view is created
* Is nothing more than a **getting the value** and **setting the value** (getter and setter)
* We need to pass the **dollar** / **money sign** to access the binding within the property wrapper
* When we pass a value from parent to child normally the child is not allowed to update that value but **if we are binding the child can then update the value back to the parent**
* The **value** inside that binding will access the **wrapped value**
* `Binding<T>`
* With custom bindings we can bind two other pieces of data to streamline our data flow and make sure everying is much more in sync
* [Create custom Bindings in SwiftUI | Advanced Learning #27](https://www.youtube.com/watch?v=K91rKH_O8BY&list=PLwvDm4Vfkdphc1LLLjCaEd87BEg07M97y&index=28)

## Observable Object

* Lets views "observer" or listen to objects of this class for changes
* By default, changes to the class aren't published
* You must specifically mark the properties that will publish changes
* Allows you to store state in an object that is independet from any views
* Use the @Published property wrapper to mark any properties you want to broadcast changes

## Environment Object

* Allows you to make an object accessible to the view you attach it to, plus all its subviews
*  From the view it is attached to and any subviews, access the environment object

## Identifiable

* Helps distinguish from different items when you're iterating through them

## Hashable

* For navigation ad the tab view to distinguish between different values as well

## Decodable

* Used for JSON parsing