# iOS Webservices (backend integration)

### Tutorials

* [API Manager or Network Model](https://github.com/Moya/Moya)
* [Moya Object Mapper](https://cocoapods.org/pods/Moya-ObjectMapper)

### API Sample Request

	curl -X GET "https://api-stg.albert.today/api/v1/pinnedtags/"
		-H "accept: application/json"
		-H "X-Auth-Token: *****a997c0f443891ea5d175a2*****"
		-H "X-CSRFToken: *****AiAYei5kQ1gHZYc01oPltzJtZeDR5gE2H6Ucr9Dw9sHWZL3cUDL4u1*****"

### API Sample Response

	[
	  {
	    "id": 4,
	    "name": "Library",
	    "company": 86,
	    "is_active": true,
	    "translations": [
	      {
	        "id": 11,
	        "pinned_tag": 4,
	        "language": "CN",
	        "name": "内容库"
	      },
	      {
	        "id": 13,
	        "pinned_tag": 4,
	        "language": "FR",
	        "name": "Bibliothèque"
	      },
	      {
	        "id": 14,
	        "pinned_tag": 4,
	        "language": "JP",
	        "name": "ライブラリー"
	      },
	      {
	        "id": 15,
	        "pinned_tag": 4,
	        "language": "KR",
	        "name": "라이브러리"
	      },
	      {
	        "id": 12,
	        "pinned_tag": 4,
	        "language": "TC",
	        "name": "內容庫"
	      }
	    ]
	  }
	]

### WebserviceAPIConstants.swift

	public enum WebserviceAPI  {
		case getPinnedTags(token: String, page: Int, pageSize: Int)
	}

### WebserviceAPI.swift

	extension WebserviceAPI: TargetType {
		
		public var parameterEncoding: ParameterEncoding {
		   	switch self {
		   		case .login, .loginContainer:
		   			return JSONEncoding.default
        		default:
        			return URLEncoding.default
		   	}
	   }
       
		public var baseURL: URL {
		   	switch self {
		   		case .download(let url): return URL(string: url)!
		   		case .getStarsFeed: return URL(string: (Shortcut.appDelegate.themeManager?.getStarsFeedServerURL())!)!
		   		
		   		// version 2 apis
				case .feeds, .pinnedFeeds:
					var url = Shortcut.appDelegate.themeManager?.getServerURL()
            		url = url?.replacingOccurrences(of: "v1", with: "v2")
            		return URL(string: url!)!
            		
            	case .loginContainer:
		            return URL(string: (Shortcut.appDelegate.themeManager?.getContainerServerURL())!)!
		      	default:
		            return URL(string: (Shortcut.appDelegate.themeManager?.getServerURL())!)!
		       }
		   	}
	   }
        
		public var path: String {
			switch self {
				case .getPinnedTags: return "pinnedtags/"
			}
		}
	
		public var method: Moya.Method {
        	switch self {
        		case .login, .loginContainer, .logout: return .post
        		case .editMediaText: return .patch
        		case .editVideoResource, .editPhotoResourceOne: return .put
        		case .deleteMedia, .deleteMediaResource: return .delete
        		default: return .get
        	}
       }
       
		public var sampleData: Data { 
		   	switch self {
		   		case .getPinnedTags:
						return Bundle.getResource(fileName: "pinnedTags", fileType: "json")
		    	}
	   }
       
	   public var task: Task {
		   	switch self {
	       	case .getPinnedTags(_, let page, let pageSize):
					return .requestParameters(
						parameters: [
							"page": page,
							"page_size": pageSize
						],
						encoding: parameterEncoding
					)
	    	}
	   }
       
	   public var headers: [String: String]? {
	    	switch self {
	        	case .getPinnedTags(let token, _, _):
	        		return ["X-Auth-Token": token]
	        }
	   }
       
	   public var validationType: ValidationType {
	   		return .successCodes
		}
        
	}

### Reponse Model

	struct PinnedTagTranslation: Mappable {
		var id: Int32? = 0
		var pinnedTagId: Int32? = 0
		var language: String? = ""
		var name: String? = ""
		
		init?(map: Map) { }
		
		mutating func mapping(map: Map) {
			id <- map["id"]
			pinnedTagId <- map["pinned_tag"]
			language <- map["language"]
			name <- map["name"]
		}
	}
	
	struct PinnedTag: Mappable {
		var id: Int32? = 0
		var name: String? = ""
		var company: Int32? = 0
		var isActive: Bool? = false
		var translations: [PinnedTagTranslation]?
		
		init?(map: Map) { }
		
		mutating func mapping(map: Map) {
			id <- map["id"]
			name <- map["name"]
			company <- map["company"]
			isActive <- map["is_active"]
			translations <- map["translations"]
		}
	}
	
	struct PinnedTagsResponse: Mappable {
		var tags: [PinnedTag]?
		var success: Bool?
		
		init?(map: Map) { }
		
		mutating func mapping(map: Map) {
			tags <- map["tags"]
			success <- map["success"]
		}
	}

### NetworkingManager.swift

	class NetworkingManager: NSObject {
		public func getPinnedTags(
			_ token: String,
			_ page: Int,
			_ pageSize: Int,
			completion: @escaping(Response<[PinnedTag]?>, Int32) -> ()) {
			
			if (Connectivity.isConnectedToInternet == false) {
				completion(Response.failed(noInternetError()), 0)
				return
			}
			
			let token = token.decrypt()
			queue.addOperation {
				WebServiceProvider.request(.getPinnedTags(
											token: token,
											page: page,
											pageSize: pageSize)
				) {
					result in
					
					switch result {
						case .success(let response):
							do {
								#if STAGING
								DebugInfo.log(info: "\(DebugInfoKey.api.rawValue) \(response.request?.debugDescription ?? "") (\(response.statusCode)), token: \(token), page: \(page), page-size: \(pageSize)")
								print(try response.mapJSON())
								print("")
								print("- - -")
								print("")
								#endif
	
								var totalCount: String = ""
								if let response = response.response {
									let dictResponseHeader = response.allHeaderFields
	
									if (dictResponseHeader.keys.contains("Count")) {
										totalCount = dictResponseHeader["Count"] as? String ?? ""
									}
									else if (dictResponseHeader.keys.contains("count")) {
										totalCount = dictResponseHeader["count"] as? String ?? ""
									}
								}
	
								guard let data = Mapper<PinnedTag>().mapArray(JSONObject: try response.mapJSON())
									else {
										throw NSError(domain: "PinnedTags",
													  code: 0,
													  userInfo: ["description": "NODATA_NO_RESULTS_FOUND".localized()])
								}
								completion(Response.succeed(data), Int32(totalCount) ?? 0)
							}
							catch _ as NSError {
								completion(Response.failed(RequestError.init(errorCode: 0, errorMessage: "")), 0)
							}
							
						case .failure(let error):
							log.debug(error.response ?? "")
							completion(Response.failed(RequestError(errorCode: error.response?.statusCode ?? 0,
																	errorMessage: error.response?.description ??
																		"GENERIC_ERROR_MESSAGE".localized())), 0)
					}
				}
			}
		}
		
	}
	
### Controller

	class NewsfeedController: BaseController {
		func getPinnedTags(
			onSuccess: @escaping ([PinnedTag]?, Int32) -> (),
			onFailure: @escaping () -> ()) {
			
			self.pinnedTags = [PinnedTag]()
			page = 1
			
			guard let user = userManager.getLoginUser() else {
				return
			}
					
			let pageLimit = 30
			Shortcut.networkManager.getPinnedTags(
				user.token ?? "",
				page,
				pageLimit) { (response, count) in
				switch response {
				case .succeed(let result):
					if let result = result {
						self.pinnedTags.append(contentsOf: result)
						onSuccess(result, count)
					}
				case .failed(let error):
					self.checkWebServiceError(errorCode: error.errorCode)
					onFailure()
				}
			}
		}
	}

### View Controller

	open override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
		super.tabBar(tabBar, didSelect: item)
        
		if let idx = tabBar.items?.firstIndex(of: item),
			let nc = viewControllers?[idx] as? UINavigationController {
		    	()
		}
		else if let newsFeedVC = nc.topViewController as? NewsfeedTableViewController {
			newsFeedVC.controller.getPinnedTags(
				onSuccess: { (feeds, count) in
					()
				},
				onFailure: {
					()
				})
			}
		}
	        
	}