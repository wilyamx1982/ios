# APNS Certificate Expiring for Mixpanel

**Received email details**

The Apple Push Notification service certificate for your Mixpanel project: **Miss Bee-Stg**, will expire on Wednesday, August 5th, at 2:27 AM. Please generate a new certificaste at [Apple's developer website](https://developer.apple.com/) and upload it to [Mixpanel](https://mixpanel.com/).

-

## Mixpanel: Get the app certificate name / bundle identifier that needs update

1. Login to your [mixpanel](https://mixpanel.com/login/) account
1. Select **Mixpanel project** (top-left dropdown list) specified in the email like [Miss Bee-Stg](https://mixpanel.com/report/1318756/insights)
1. From the **right menu (gear icon)** select the **PROJECT SETTINGS** of Mixpanel project like [Miss Bee-Stg](https://mixpanel.com/settings/project/1318756)
1. Select the **PROJECT SETTINGS** / [Messages](https://mixpanel.com/settings/project/1318756#message)
1. Look for **Apple Push Certificate** app bundle identifier like `com.thinkittwice.jennyA-staging`

## Apple Developer: Generate APNS Certificate via app bundle identifier

1. Generate a [Certificate Signing Request (CSR)](https://help.apple.com/developer-account/#/devbfa00fef7) via Keychain Access
	* Launch the Keychain Access application in your Mac OSX
	* Select **Keychain Access** -> **Certificate Assistant** -> **Request a Certificate From a Certificate Authority**
	* **Suggested filenames:** `albert2-apns`, `albert2-apns-dev`, `albert-apns`, `albert-apns-dev`
	* `aps.cer`, `aps_development.cer`
1. Login to your [apple account](https://developer.apple.com/)
1. Go to [identifiers](https://developer.apple.com/account/resources/identifiers/list) list and select based on bundle identifier like `com.thinkittwice.jennyA-staging`
1. Select **[Edit]** button from **Capabilities / Push Notifications**
1. **[Create Certificate]** that has expiration displayed
1. **[Choose File]** the generated CSR file from #1 and **[continue]**
1. **[Download] the certificate** file `aps_development.cer`
1. **Double-click the certificate** file above to install in Keychain Access
1. **Open Keychain Access**
1. **Select the generated certificate** for apns like `Apple Development IOS Push Services: com.thinkittwice.jennyA-staging` and **[export]** to **generate the p12 file.** **Generate certificate** that has **NO PASSPHRASE** as requirement to mixpanel certificate.
	
	**Suggested filenames:** `albert-apns.p12`, `ugo-apns.p12`, `energia-apns.p12`

## Test the p12 file for device push notification (optional)

1. **Generate pem file** from p12 file

		openssl pkcs12 -in jennyA-stg-apns.p12 -out jennyA-stg-apns.pem -nodes -clcerts

1. **Login the user via app** using bundle identifier
1. **Get the device token** using `PoleLuxe_user.id`
1. **Manual device notification** via [APNS Tester](https://www.apnstester.com/apns/)
1. The device **expected to receive device push notification** 
 
## Mixpanel: Upload the p12 File

1. Login to your [mixpanel](https://mixpanel.com/login/) account
1. Select **Mixpanel project** specified in the email like [Miss Bee-Stg](https://mixpanel.com/report/1318756/insights)
1. From the **right menu (gear icon)** select the **PROJECT SETTINGS** of Mixpanel project like [Miss Bee-Stg](https://mixpanel.com/settings/project/1318756)
1. Select the **PROJECT SETTINGS** / [Messages](https://mixpanel.com/settings/project/1318756#message)
1. Look for **Apple Push Certificate** and **Upload p12 File**
1. Mixpanel should accept and [Upload p12 File]